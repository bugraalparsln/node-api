FROM node:20-alpine3.18
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY --chown=node:node . .

USER node
RUN npm install

EXPOSE 8080

CMD ["/bin/sh",  "-c", \
    "if [ ${NODE_ENV} == \"test\" ] ; then node --env-file .env.test server.js ; \
    else node --env-file .env server.js ; fi"]
